﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviourPunCallbacks
{
    public GameObject m_LobbyButton;
    public GameObject m_MainPanel;
    public GameObject m_LobbyPanel;
    public InputField m_NickName; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(m_NickName.text!="")
        {
            m_LobbyButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            m_LobbyButton.GetComponent<Button>().interactable = false;
        }
    }
    public void GoToLobby()
    {
        PhotonNetwork.ConnectUsingSettings();
        
    }
    public override void OnConnectedToMaster()
    {
        m_MainPanel.SetActive(false);
        m_LobbyPanel.SetActive(true);
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinLobby();
    }
}
